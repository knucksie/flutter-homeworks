import 'package:flutter/material.dart';

class PlainChat extends StatefulWidget {
  const PlainChat({Key? key}) : super(key: key);

  @override
  _PlainChatState createState() => _PlainChatState();
}

class _PlainChatState extends State<PlainChat> {
  List<String> _messages = ['first', 'second'];

  void _sendMessage(String message) {
    setState(() {
      _messages.add(message);
    });
  }

  TextEditingController _textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Expanded(
              child: ListView(
                children: _messages.map((item) {
                  return ListTile(
                    title: Row(
                      children: [
                        Flexible(
                          child: Container(
                            margin: const EdgeInsets.all(5.0),
                            padding: const EdgeInsets.all(10.0),
                            decoration: BoxDecoration(
                              gradient: LinearGradient(colors: [
                                Color(0xffDA44bb),
                                Color(0xff8921aa),
                              ]),
                              border: Border.all(width: 0),
                              borderRadius: BorderRadius.all(Radius.circular(
                                      30.0) //                 <--- border radius here
                                  ),
                            ),
                            child: Text(
                              item,
                              textAlign: TextAlign.right,
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                }).toList(),
              ),
              flex: 4,
            ),
            Expanded(
              child: Row(children: [
                Expanded(
                  child: TextField(
                    controller: _textEditingController,
                    decoration: InputDecoration(
                      helperText: 'Write something',
                      filled: true,
                      fillColor: Colors.purple.shade100,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                    ),
                  ),
                  flex: 7,
                ),
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 20.0),
                    margin: const EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 0.0),
                    child: FloatingActionButton(
                      child: Icon(Icons.chat),
                      onPressed: () {
                        _sendMessage(_textEditingController.text);
                        _textEditingController.clear();
                      },
                    ),
                  ),
                  flex: 1,
                ),
              ]),
            ),
          ],
        ),
      ),
    ));
  }
}
