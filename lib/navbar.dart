import 'package:flutter/material.dart';
import 'main.dart';

class NavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          ListTile(
            leading: Icon(Icons.chat),
            title: Text('Homework: Plain chat'),
            onTap: () =>
                Navigator.push(context, MaterialPageRoute(builder: (context) {
              return MyHomePage(title: 'Welcome to NaxiChat');
            })),
            trailing: ClipOval(
              child: Container(
                color: Colors.green,
                width: 30,
                height: 30,
                child: Center(
                  child: Text(
                    '№1',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
